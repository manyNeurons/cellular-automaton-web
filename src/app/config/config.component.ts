import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {

  constructor() { }


  init_config = [];
  init_num: number = 100;


  ngOnInit(): void { 
    for(let i=0; i<this.init_num; i++) {
      this.init_config.push('');
    }
  }


  ngAfterContentInit(): void {  }

}
