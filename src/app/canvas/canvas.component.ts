import { Component, OnInit } from '@angular/core';
import * as p5 from 'p5';
import d1CelAutom from './d1CelAutom';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit {

  constructor() { }
  
  private canvas;
  private p5;
  private cel = new d1CelAutom();

  private tam: number = 0;
  //there has to be a minimum number of 'inicial config numbers'
  
  private len: number;
  private y = 0;
  private x = 0;
  
  private regla = [];
  private num = 30;
  
  ngOnInit(): void {

    const sketch = (p5) => {

      
      var ACin = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
      var init_num: number = 100;


      const setButtonVC = (cx:string, v:number) => {
     
        if(v == 0){
          (<HTMLInputElement>document.getElementById(cx)).className = "button white";
          (<HTMLInputElement>document.getElementById(cx)).value = v.toString();
        }else if(v == 1){
          (<HTMLInputElement>document.getElementById(cx)).className = "button black";
          (<HTMLInputElement>document.getElementById(cx)).value = v.toString();
        }
        
      }

      const updateButtons = () => {
        if(Number.isInteger(this.num)){
          this.regla = this.cel.ruleList(this.num);
          setButtonVC('c0', this.regla[0][3]);
          setButtonVC('c1', this.regla[1][3]);
          setButtonVC('c2', this.regla[2][3]);
          setButtonVC('c3', this.regla[3][3]);
          setButtonVC('c4', this.regla[4][3]);
          setButtonVC('c5', this.regla[5][3]);
          setButtonVC('c6', this.regla[6][3]);
          setButtonVC('c7', this.regla[7][3]);
        }
        //lo que vale cada config
        var c0 = Number((<HTMLInputElement>document.getElementById("c0")).value);
        var c1 = Number((<HTMLInputElement>document.getElementById("c1")).value);
        var c2 = Number((<HTMLInputElement>document.getElementById("c2")).value);
        var c3 = Number((<HTMLInputElement>document.getElementById("c3")).value);
        var c4 = Number((<HTMLInputElement>document.getElementById("c4")).value);
        var c5 = Number((<HTMLInputElement>document.getElementById("c5")).value);
        var c6 = Number((<HTMLInputElement>document.getElementById("c6")).value);
        var c7 = Number((<HTMLInputElement>document.getElementById("c7")).value);
  
        (<HTMLInputElement>document.getElementById('c0')).onclick = function() {
          c0 = (c0 -1)*(-1);
          setButtonVC('c0', c0);
        }​;​
        (<HTMLInputElement>document.getElementById('c1')).onclick = function() {
          c1 = (c1 -1)*(-1);
          setButtonVC('c1', c1);
        }​;​
        (<HTMLInputElement>document.getElementById('c2')).onclick = function() {
          c2 = (c2 -1)*(-1);
          setButtonVC('c2', c2);
        }​;​
        (<HTMLInputElement>document.getElementById('c3')).onclick = function() {
          c3 = (c3 -1)*(-1);
          setButtonVC('c3', c3);
        }​;
        (<HTMLInputElement>document.getElementById('c4')).onclick = function() {
          c4 = (c4 -1)*(-1);
          setButtonVC('c4', c4);
        }​;​​
        (<HTMLInputElement>document.getElementById('c5')).onclick = function() {
          c5 = (c5 -1)*(-1);
          setButtonVC('c5', c5);
        }​;​
        (<HTMLInputElement>document.getElementById('c6')).onclick = function() {
          c6 = (c6 -1)*(-1);
          setButtonVC('c6', c6);
        }​;​
        (<HTMLInputElement>document.getElementById('c7')).onclick = function() {
          c7 = (c7 -1)*(-1);
          setButtonVC('c7', c7);
        }​;​
      }


      p5.setup = () => {
        this.len = ACin.length;
        document.getElementById("canvas-component").style.height = String(p5.windowHeight+1)+"px";
        document.getElementById("canvas-component").style.width = String(p5.windowWidth)+"px";
        this.canvas = p5.createCanvas(p5.windowWidth, p5.windowHeight);
        // this.canvas.style('width', 'auto');
        // this.canvas.style('height', 'auto');

        console.log(p5.width+"  "+p5.height)
        console.log(document.getElementById("canvas-component").clientHeight);
        this.canvas.parent("canvas-component");
        // let sX = (<HTMLInputElement>document.getElementById("canvas-component").style.width;
        // let sY = (<HTMLInputElement>document.getElementById("canvas-component").style.height;
        // console.log(sX,sY,0)
        // p5.resizeCanvas(sX, sY);
        // this.canvas.position(20, p5.windowHeight*0.2);
        //this.canvas.style('z-index', '-1');
        p5.background(255);        
        
        this.tam = p5.width / this.len;
        p5.frameRate(0); 

        //stop y play
        (<HTMLInputElement>document.getElementById('stap')).onclick = function() {
          if((<HTMLInputElement>document.getElementById('stap')).value == 'playd'){
            p5.frameRate(0);
            (<HTMLInputElement>document.getElementById('stap')).value = 'stopd';
          }else{
            p5.frameRate(30);
            (<HTMLInputElement>document.getElementById('stap')).value = 'playd';
          }
        }​;​

        //initial wolfram config
        updateButtons();

        //initial init config buttons
        for(let i=0; i<init_num; i++) {
          let cx = 'd'+i;
          let v = Number((<HTMLInputElement>(<HTMLInputElement>document.getElementById(cx))).value);
          (<HTMLInputElement>document.getElementById(cx)).onclick = function() {
            v = (ACin[i] -1)*(-1);
            setButtonVC(cx,v);
            ACin[i] = v;
          }
        }

        //reset ACin!
        (<HTMLInputElement>document.getElementById('reset')).onclick = function() {
          for(let i=0; i<init_num; i++) {
            ACin[i] = 0;
            setButtonVC('d'+i,0);
          }
        }​;​
        
        //empieza a dibujar desde abajo        
        this.y = p5.height-15-this.tam;

      }

      
      p5.draw = () => {

        //mientras no se varie el valor en el html no cambia el 
        // valor de la this.regla ni el de los propios botones
        if(this.num != Number((<HTMLInputElement>document.getElementById("rule")).value) 
        // || 
        //   (<HTMLInputElement>document.getElementById('c1')).clicked == true ||
        //   (<HTMLInputElement>document.getElementById('c2')).clicked == true ||
        //   (<HTMLInputElement>document.getElementById('c3')).clicked == true ||
        //   (<HTMLInputElement>document.getElementById('c4')).clicked == true ||
        //   (<HTMLInputElement>document.getElementById('c5')).clicked == true ||
        //   (<HTMLInputElement>document.getElementById('c6')).clicked == true ||
        //   (<HTMLInputElement>document.getElementById('c7')).clicked == true 
          )
          {
          this.num = Number((<HTMLInputElement>document.getElementById("rule")).value);
          updateButtons();
        }
        // console.log(this.num)
        
        
        //init config buttons update
        for(let i=0; i<init_num; i++) {
          let cx = 'd'+i;
          setButtonVC(cx,ACin[i]);
        }

        
        let c0 = Number((<HTMLInputElement>document.getElementById("c0")).value);
        let c1 = Number((<HTMLInputElement>document.getElementById("c1")).value);
        let c2 = Number((<HTMLInputElement>document.getElementById("c2")).value);
        let c3 = Number((<HTMLInputElement>document.getElementById("c3")).value);
        let c4 = Number((<HTMLInputElement>document.getElementById("c4")).value);
        let c5 = Number((<HTMLInputElement>document.getElementById("c5")).value);
        let c6 = Number((<HTMLInputElement>document.getElementById("c6")).value);
        let c7 = Number((<HTMLInputElement>document.getElementById("c7")).value);

        //actualiza el valor del número 
        this.num = this.cel.ruleListInv(''+c0+c1+c2+c3+c4+c5+c6+c7);
        (<HTMLInputElement>document.getElementById("rule")).value = this.num.toString();
        (<HTMLInputElement>document.getElementById("ruleB")).value = ''+c7+c6+c5+c4+c3+c2+c1+c0;
        // console.log(''+c0+c1+c2+c3+c4+c5+c6+c7);

        this.x = 0;
        //velocidad del scroll afectada por los frames en los que se dibuja (smooooth!!!!!!1)
        if(p5.frameCount % 2 == 0){
          ACin.forEach(sq => {
            if(sq == 1) {
              p5.fill(sq*(-240) + 255);
              p5.square(this.x, this.y, this.tam-0.7);
              p5.noStroke();
            }
            this.x += this.tam;
          });
          //siguiente configuración
          ACin = this.cel.nextConfig(ACin, this.regla);

          
          this.regla[0][3] = (<HTMLInputElement>document.getElementById("c0")).value;
          this.regla[1][3] = (<HTMLInputElement>document.getElementById("c1")).value;
          this.regla[2][3] = (<HTMLInputElement>document.getElementById("c2")).value;
          this.regla[3][3] = (<HTMLInputElement>document.getElementById("c3")).value;
          this.regla[4][3] = (<HTMLInputElement>document.getElementById("c4")).value;
          this.regla[5][3] = (<HTMLInputElement>document.getElementById("c5")).value;
          this.regla[6][3] = (<HTMLInputElement>document.getElementById("c6")).value;
          this.regla[7][3] = (<HTMLInputElement>document.getElementById("c7")).value;
          
        }
        //scroll para arriba cuando llegue al final de la página
        // if( this.y < (p5.height-30-this.tam)){
          
        //   ACin.forEach(sq => {
        //     if(sq == 1) {
        //       p5.fill(sq*(-240) + 255);
        //       p5.square(this.x, this.y, this.tam);
        //       p5.noStroke();
        //     }
        //     this.x += this.tam;
            
        //   });
        //   this.y += this.tam/4;
        // }else{ 
          //copia y pega todo el canvas una celula más arriba
          p5.copy(0, 0, p5.windowWidth, p5.windowHeight, 0, -(this.tam)/2, p5.windowWidth, p5.windowHeight); 
        // }

      }

      // p5.windowResized = () => {
      //   p5.resizeCanvas(p5.windowWidth-20, p5.windowHeight/1.2);
      // }
    }

    this.p5 = new p5(sketch);
  }

}
